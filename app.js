const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')
const orderRoutes = require('./routes/orderRoutes')

const app = express();
const port = process.env.PORT || 4000;

// Middlewares

app
.use(cors())
.use(express.json())
.use(express.static(process.cwd() + '/public'))
.use(express.urlencoded({extended: true}));

// URIs
app
.use("/users",userRoutes)
.use("/products",productRoutes)
.use("/orders",orderRoutes);


// Mongoose connection
mongoose.connect('mongodb+srv://admin1234:admin1234@zuitt-batch197.fk6bhtr.mongodb.net/s42-s46?retryWrites=true&w=majority',{
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const db = mongoose.connection;

db.on('error',() => console.error('Connection error'));
db.once('open', () => console.log('Connected to MongoDb'));

app.listen(port, () => console.log(`API is now online at port${port}`));
