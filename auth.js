const jwt = require('jsonwebtoken');
const secret = 'capstone2SecretKey';
const UsersModel = require('./models/User')

function createAccessToken(user) {

    const data = {
        id: user.id,
        email: user.email,
        isAdmin: user.isAdmin
    }

    return jwt.sign(data,secret,{});

};

async function forgotPasswordToken(email) {

    try {
        let userInit = await UsersModel.findByEmail(email);
        if(userInit){
            let token = jwt.sign({email: email},secret,{expiresIn: 60});
            return token;
        }else{
            return {error: 'Email not found or request is invalid.'}
        }
    
    } catch (error) {
        console.log(error)
        return error
    }

};


function verify(req,res,next) {

    let token = req.headers.authorization;

    if(typeof token !== undefined){

        const tokenConfirm = verifyToken(token)
        if(tokenConfirm){
            return next()
        }
        
    }
        return res.status(401).send({auth: "failed"})

}

async function decode(token) {

    if(typeof token !== undefined){

        const tokenConfirm = verifyToken(token)

        if(tokenConfirm){
            // return jwt.decode(tokenConfirm,{complete: true}.payload)
            return tokenConfirm
        }

    }else{
        return null
    }
}

function verifyToken(token) {

    token = token.includes("Bearer ") ? token.slice(7, token.length) : token;

    return jwt.verify(token, secret, (error, data) => {
        if(error){
            return false
        }else{
            return data
        }
    })
}




module.exports = {
    createToken: createAccessToken,
    forgotPasswordToken: forgotPasswordToken,
    verify: verify,
    decode: decode
}