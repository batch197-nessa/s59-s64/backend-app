const mongoose = require('mongoose');


const productSchema = {
    name: {
        type: String,
        required: [true, "Category Name is required"]
    },
    description: {
        type: String,
        required: [true, "Description is required"]
    },
    price: {
        type: Number,
        required: [true, "Price is required"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    imagePath: {
        type: String,
        default: ''
    }

}

module.exports = mongoose.model("Product",productSchema)