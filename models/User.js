const mongoose = require('mongoose');


const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, "First Name is required"]
    },
    lastName: {
        type: String,
        required: [true, "Last Name is required"]
    },
    email: {
        type: String,
        unique: true,
        required: [true, "Email is required"]
    },
    password: {
        type: String,
        required: [true, "Password is required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    mobileNo: {
        type: String,
        required: [true, "Mobile Number is required"]
    },
    address: {
        type: String,
        default:''
    }
  
})

class UserClass {
    static findByEmail(email) {
        return this.findOne({ email });
      }
}

userSchema.loadClass(UserClass)


module.exports = mongoose.model("User", userSchema);
